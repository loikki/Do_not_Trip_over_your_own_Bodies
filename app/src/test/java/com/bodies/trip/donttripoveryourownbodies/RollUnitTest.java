package com.bodies.trip.donttripoveryourownbodies;

import org.junit.Test;
import static org.junit.Assert.*;

public class RollUnitTest {
    @Test
    public void testRoll100() {
        Roll roll = new Roll();
        assertEquals( 100, roll.roll100());
    }
}
