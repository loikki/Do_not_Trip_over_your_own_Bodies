package com.bodies.trip.donttripoveryourownbodies;

enum Difficulty {
    regular,
    hard,
    extreme
}

enum Test {
    failed,
    passed,
    critical_success,
    critical_failed
}

public class Roll {
    /* Init */
    Roll(int seed) {}
    Roll() {}

    /* Methods */

    /**
     * Roll a d100
     * @return dice value [1, 100]
     */
    public int roll100() {
        return 100;
    }

    /**
     * Do a skill/attribute test
     * @param value Attribute value
     * @param diff Test difficulty
     * @return if test passed
     */
    public Test doTest(int value, Difficulty diff) {
        int required_roll = getRequiredRoll(value, diff);
        int score = roll100();

        /* Critical success */
        if (score == 1)
            return Test.critical_success;

        /* Critical failure */
        if (score == 99 ||
                (required_roll < 50 && score > 95))
            return Test.critical_failed;

        /* Failure */
        if (score > required_roll)
            return Test.failed;

        /* Success */
        return Test.passed;
    }

    /**
     * Get the threshold corresponding to the test
     * @param value Regular value to reach
     * @param diff Test difficulty
     * @return threshold at required difficulty
     */
    public int getRequiredRoll(int value, Difficulty diff) {
        /* Regular difficulty */
        if (diff == Difficulty.regular)
            return value;
        /* Hard difficulty */
        else if (diff == Difficulty.hard)
            return value / 2;
        /* Extreme Difficulty */
        else if (diff == Difficulty.extreme)
            return value / 5;

        throw new IllegalArgumentException();
    }

}
