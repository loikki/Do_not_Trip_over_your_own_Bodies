package com.bodies.trip.donttripoveryourownbodies;

public class Character {

    /* Init */
    Character() {
        this.strength = 0;
        this.dexterity = 0;
        this.constitution = 0;
        this.appearance = 0;
        this.intelligence = 0;
        this.power = 0;
        this.education = 0;
        this.luck = 0;

        this.roll = new Roll();
    }

    /* Attribute checks */

    /**
     * Do a strength test
     * @param diff Test difficulty
     * @return Test result
     */
    public Test testStrength(Difficulty diff) {
        return this.roll.doTest(this.strength, diff);
    }

    /**
     * Do a dexterity test
     * @param diff Test difficulty
     * @return Test result
     */
    public Test testDexterity(Difficulty diff) {
        return this.roll.doTest(this.dexterity, diff);
    }

    /**
     * Do a constitution test
     * @param diff Test difficulty
     * @return Test result
     */
    public Test testConstitution(Difficulty diff) {
        return this.roll.doTest(this.constitution, diff);
    }

    /**
     * Do an appearance test
     * @param diff Test difficulty
     * @return Test result
     */
    public Test testAppearance(Difficulty diff) {
        return this.roll.doTest(this.appearance, diff);
    }

    /**
     * Do an intelligence test
     * @param diff Test difficulty
     * @return Test result
     */
    public Test testIntelligence(Difficulty diff) {
        return this.roll.doTest(this.intelligence, diff);
    }

    /**
     * Do a power test
     * @param diff Test difficulty
     * @return Test result
     */
    public Test testPower(Difficulty diff) {
        return this.roll.doTest(this.power, diff);
    }

    /**
     * Do an education test
     * @param diff Test difficulty
     * @return Test result
     */
    public Test testEducation(Difficulty diff) {
        return this.roll.doTest(this.education, diff);
    }

    /**
     * Do a luck test
     * @param diff Test difficulty
     * @return Test result
     */
    public Test testLuck(Difficulty diff) {
        return this.roll.doTest(this.luck, diff);
    }

    /* Attributes */

    /**
     * Character strength
     */
    private int strength;
    /**
     * Character dexterity
     */
    private int dexterity;
    /**
     * Character constitution
     */
    private int constitution;
    /**
     * Character appearance
     */
    private int appearance;
    /**
     * Character intelligence
     */
    private int intelligence;
    /**
     * Character power
     */
    private int power;
    /**
     * Character education
     */
    private int education;
    /**
     * Character luck
     */
    private int luck;
    /**
     * Roll instance for tests
     */
    private Roll roll;
}
